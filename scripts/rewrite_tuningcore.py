"""
Rewrites a tuningcore from Malte into something that is ready to be passed into scmcallib
"""

import re
from os.path import exists

import click


@click.command()
@click.argument('src')
@click.argument('dst')
def hello(src, dst):
    if not exists(src):
        raise click.UsageError("No such file {}".format(src))


    with open(src) as fh:
        data = fh.read()

        # Below are a number of regex replacements to make on the file
        # CABLE_WITHIN_CABLE_rcp85_CN1_fnLosses.mat => fnLosses_WITHIN_$DATA_FOLDER/CABLE_RCP85-CN1_tuning.MAG
        res = re.sub(r"CABLE_WITHIN_CABLE_rcp85_([CN10]*)_([a-zA-Z]*).mat",
                     r"\2_WITHIN_CABLE_RCP85-\1_tuning.MAG", data)
        # _GPP_ to _NPP_
        res = re.sub(r"_GPP", r"_NPP", res)
        # _TASDRIVEN to -TAS
        res = re.sub(r",([a-zA-Z0-9_]+)_TASDRIVEN", lambda m: "," + m.group(1).upper().replace("_", "-") + "-TAS", res)

    # Write out the corrected file
    with open(dst, 'w') as fh:
        fh.write(res)


if __name__ == '__main__':
    hello()
