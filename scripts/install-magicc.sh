#!/usr/bin/env bash

# Downloads and extracts a version of MAGICC for use in testing
# An optional version number (ie v7.0.1) can be passed to this script otherwise
# the current master branch of MAGICC is used.

MAGICC_ARTIFACTS_URL="https://s3-us-west-2.amazonaws.com/magicc-artifacts-prod"
MAGICC_VERSION=${1:-b_master}
TARGET_DIRECTORY=${2:-/opt/magicc-$MAGICC_VERSION}

MAGICC_FILENAME=magicc-$MAGICC_VERSION.tar.gz
curl $MAGICC_ARTIFACTS_URL/build/$MAGICC_FILENAME --output $MAGICC_FILENAME
mkdir -p $TARGET_DIRECTORY
tar -xzf $MAGICC_FILENAME -C $TARGET_DIRECTORY

echo "MAGICC version $MAGICC_VERSION was installed to $TARGET_DIRECTORY"
