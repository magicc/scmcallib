import json
from os.path import join, dirname
from unittest.mock import MagicMock

import numpy as np
import pytest
from scmdata import ScmDataFrame

from scmcallib import PointEstimateFinder, ParameterSet, DistributionFinder as DF
from scmcallib.distributions import Scalar
from scmcallib.scm import BaseSCM

TEST_DATA_DIR = join(dirname(__file__), "test_data")


def pytest_addoption(parser):
    parser.addoption(
        "--runslow", action="store_true", default=False, help="run slow tests"
    )


def pytest_configure(config):
    config.addinivalue_line("markers", "slow: mark test as slow to run")


def pytest_collection_modifyitems(config, items):
    if config.getoption("--runslow"):
        # --runslow given in cli: do not skip slow tests
        return
    skip_slow = pytest.mark.skip(reason="need --runslow option to run")
    for item in items:
        if "slow" in item.keywords:
            item.add_marker(skip_slow)


def build_fake_ts(t, a, b, c, noise_stdv=1.0):
    noise = np.random.normal(0, noise_stdv, size=len(t))
    return ScmDataFrame(
        a * np.power(t, 2) + b * t + c + noise,
        columns={
            "model": ["unspecified"],
            "climate_model": ["fake"],
            "scenario": ["unspecified"],
            "region": ["World"],
            "variable": ["Surface Temperature"],
            "unit": ["K"],
        },
        index=t,
    )


class FakeScm(BaseSCM):
    def _run_single(self, parameters, variables=None):
        a = parameters.get("a", 1.0)
        b = parameters.get("b", 10.0)
        c = parameters.get("c", 100.0)

        t = np.arange(1900, 2100)
        return build_fake_ts(t, a, b, c)


@pytest.fixture(scope="function")
def point_finder():
    ps = ParameterSet()
    ps.set_config("test-config", "value")
    ps.set_tune("test-tune", Scalar(4))

    return PointEstimateFinder(ps, reference_period=None)


@pytest.fixture(scope="function")
def mock_scm(target_ts):
    scm = BaseSCM()
    scm._run_single = MagicMock()
    scm._run_single.return_value = target_ts

    return scm


@pytest.fixture(scope="function")
def target_ts():
    data = np.arange(20).reshape((10, 2))
    yield ScmDataFrame(
        data,
        columns={
            "model": ["a_iam"],
            "climate_model": ["a_model"],
            "scenario": ["a_scenario"],
            "region": ["World"],
            "variable": ["Surface Temperature", "SLR_EXPANSION"],
            "unit": ["K", "mm"],
        },
        index=range(2000, 2010),
    )


@pytest.fixture(scope="function")
def target_ts_multiple_sce():
    data = np.arange(20).reshape((10, 2))
    yield ScmDataFrame(
        data,
        columns={
            "model": ["a_iam"],
            "climate_model": ["a_model"],
            "scenario": ["a_scenario", "a_scenario2"],
            "region": ["World"],
            "variable": ["Surface Temperature"],
            "unit": ["K"],
        },
        index=range(2000, 2010),
    )


@pytest.fixture(scope="function")
def tuningcore():
    with open(join(TEST_DATA_DIR, "tuningcore_example.json")) as fh:
        yield json.load(fh)


@pytest.fixture(scope="module")
def DistributionFinder():
    if not DF:
        pytest.skip("pymc3 not installed")
    return DF
