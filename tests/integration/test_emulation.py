import numpy as np
import pytest
from conftest import FakeScm, build_fake_ts

from scmcallib import PointEstimateFinder, ParameterSet
from scmcallib.distributions import Uniform


def kind_of_close(a, b, rel_tol=0.1):
    return abs(a - b) / min(abs(a), abs(b)) < rel_tol


@pytest.mark.slow
@pytest.mark.parametrize(
    "opt_kwargs",
    (
        {"optimiser_name": "scipy", "options": {"eps": 1e-5}},
        {"optimiser_name": "bayesopt", "init_points": 100, "n_iter": 10, "init_method": "lhs"},
    ),
)
def test_simple_emulation(opt_kwargs):
    # Set up the prior knowledge we have about the parameters that are going to be emulated
    best_guess_a = 2.5
    best_guess_b = 1
    best_guess_c = 1000

    t = np.arange(1990, 2100)
    target = build_fake_ts(t, best_guess_a, best_guess_b, best_guess_c)

    param_set = ParameterSet()
    param_set.set_tune("a", Uniform(lower=best_guess_a * 0.5, upper=best_guess_a * 2))
    param_set.set_tune("b", Uniform(lower=best_guess_b * 0.5, upper=best_guess_b * 2))
    param_set.set_tune("c", Uniform(lower=best_guess_c * 0.5, upper=best_guess_c * 2))

    emulator = PointEstimateFinder(
        param_set, reference_period=(2000, 2010), sample_step=10
    )
    emulator.set_target(target)

    with FakeScm() as scm:
        if opt_kwargs["optimiser_name"] == "scipy":
            x0 = emulator.find_x0(scm, samples=200)
            opt_kwargs["x0"] = x0

        results = emulator.find_best_fit(scm, **opt_kwargs)

        assert kind_of_close(best_guess_a, results.coeffs["a"], rel_tol=0.3)
        assert kind_of_close(best_guess_b, results.coeffs["b"], rel_tol=1)
        assert kind_of_close(best_guess_c, results.coeffs["c"], rel_tol=1)
