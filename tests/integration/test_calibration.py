import matplotlib

matplotlib.use("Agg")

from os.path import join
import pytest
import pandas as pd
import numpy.testing as npt
from scmcallib import ParameterSet, Normal, Bound
from conftest import TEST_DATA_DIR
from scmcallib.scm import AR5IR


def read_input_data():
    gmt_file = join(TEST_DATA_DIR, "HadCRUT.4.6.0.0.annual_ns_avg_clean.csv")
    # the observable we want calibrate to:
    # global mean temperature from 1850 to 2017
    # we use the last year of observations as the reference year here.
    observed_gmt = pd.read_csv(gmt_file, index_col=0)
    observed_gmt.index = [int(i.split("-")[0]) for i in observed_gmt.index.values]
    observed_gmt -= observed_gmt.loc[2017]

    return observed_gmt.loc[1850:2017]


@pytest.mark.slow
def test_simple_calibration(DistributionFinder):
    # small sample size for testing, increase for real results
    best_guess_c1 = 0.631
    best_guess_c2 = 0.429
    best_guess_a1 = 0.2240

    param_set = ParameterSet()
    param_set.set_tune("c1", Bound(Normal(mu=best_guess_c1, sd=1.0), lower=0.1))
    param_set.set_tune("c2", Bound(Normal(mu=best_guess_c2, sd=0.1), lower=0.1))
    param_set.set_tune(
        "a1", Bound(Normal(mu=best_guess_a1, sd=0.1), lower=0.0, upper=0.4)
    )

    # TODO: Update the reference period
    with AR5IR() as scm:
        calibrator = DistributionFinder(param_set, scm=scm, reference_period=2017)
        calibrator.set_target(
            read_input_data(), df_kwargs={"variable": "Surface Temperature"}
        )

        # Run the actual calibrator
        calibrator.sample(draws=100, chains=2, tune=50)

        df = calibrator.summary()

    npt.assert_allclose(0.9, df["mean"]["c1"], rtol=0.1)
    npt.assert_allclose(0.43, df["mean"]["c2"], rtol=0.1)
