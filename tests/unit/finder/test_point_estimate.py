from copy import deepcopy
from datetime import datetime
from unittest.mock import Mock, patch

import numpy as np
import numpy.testing as npt
import pandas as pd
import pytest
from scmdata import ScmDataFrame

from scmcallib import ParameterSet, Normal
from scmcallib.finder.point_estimate import (
    EmulationResult,
    PointEstimateFinder,
    Evaluator,
)
from scmcallib.metrics import LARGE_NEG_ERR, metrics
from scmcallib.scm import AR5IR
from scmcallib.utils import create_iam_dataframe, prepare_dataframe


@pytest.mark.skip
def test_valid_regression():
    mock_result = Mock(success=True)
    e = EmulationResult(mock_result, None, [1])

    assert len(e) == 1


def test_failing_regression():
    mock_result = Mock(success=False)

    with pytest.raises(AssertionError):
        EmulationResult(mock_result, None, None)


@patch("scmcallib.finder.point_estimate.get_metric")
def test_metric_lookup(mock_get_metric):
    ps = ParameterSet()
    PointEstimateFinder(ps)
    mock_get_metric.assert_called_with("neg_mean_sq_err")

    PointEstimateFinder(ps, metric="test-metric")
    mock_get_metric.assert_called_with("test-metric")


@patch("scmcallib.finder.point_estimate.get_optimiser")
def test_run_before_target(mock_optimiser):
    ps = ParameterSet()
    e = PointEstimateFinder(ps)
    scm = AR5IR()

    with pytest.raises(
        AssertionError,
        match="set_target must be called before the fit can be performed",
    ):
        e.find_x0(scm)

    with pytest.raises(
        AssertionError,
        match="set_target must be called before the fit can be performed",
    ):
        e.find_best_fit(scm)

    e.set_target(
        pd.Series(np.arange(10), index=np.arange(2000, 2010)),
        variable="Surface Temperature",
    )
    res = e.find_x0(scm)
    e.find_best_fit(scm, [], x0=res)

    mock_optimiser.assert_called_once()


def test_reference_period():
    ps = ParameterSet()
    df = create_iam_dataframe(pd.Series(np.arange(11, 21), index=range(2000, 2010)))

    e = PointEstimateFinder(ps, reference_period=2000)
    e.set_target(df)

    npt.assert_almost_equal(e.target.loc[datetime(2000, 1, 1)].sum(), 0)

    e = PointEstimateFinder(ps, reference_period=(2000, 2005))
    e.set_target(df)
    npt.assert_almost_equal(
        np.squeeze(e.target.loc[datetime(2000, 1, 1) : datetime(2005, 12, 31)].sum()), 0
    )

    # Check off the end of the dates
    e = PointEstimateFinder(ps, reference_period=(1990, 2005))
    e.set_target(df)
    npt.assert_almost_equal(
        np.squeeze(e.target.loc[datetime(1990, 1, 1) : datetime(2005, 12, 31)].sum()), 0
    )


def test_iter_over(mock_scm):
    f = PointEstimateFinder(ParameterSet())
    obs = ScmDataFrame(
        pd.DataFrame(np.arange(10).reshape((5, 2)), index=range(2000, 2005)),
        columns={
            "unit": ["K"],
            "variable": ["Surface Temperature"],
            "region": ["World"],
            "model": ["unspecified"],
            "scenario": ["RCP45", "RCP85"],
        },
    )

    f.set_target(obs, iter_over="scenario")
    mock_scm._run_single.side_effect = [
        create_iam_dataframe(
            np.arange(5), index=range(2000, 2005), variable="Surface Temperature"
        ),
        create_iam_dataframe(
            np.arange(5, 10), index=range(2000, 2005), variable="Surface Temperature"
        ),
    ]

    e = f._get_evaluator(mock_scm)
    e()
    assert mock_scm._run_single.call_count == 2


@pytest.mark.parametrize(
    "scm_out",
    (
        # variable types don't match
        create_iam_dataframe(
            np.arange(5), index=range(2000, 2005), variable="air_temperature"
        ),
        # no overlapping years
        create_iam_dataframe(
            np.arange(5), index=range(1990, 1995), variable="Surface Temperature"
        ),
        # wrong region
        create_iam_dataframe(
            np.arange(5),
            index=range(2000, 2005),
            variable="Surface Temperature",
            region="World|Southern Hemisphere",
        ),
    ),
)
def test_missing_values(mock_scm, scm_out):
    f = PointEstimateFinder(ParameterSet())
    obs = ScmDataFrame(
        pd.DataFrame(np.arange(5), index=range(2000, 2005)),
        columns={
            "unit": ["K"],
            "variable": ["Surface Temperature"],
            "region": ["World"],
            "model": ["unspecified"],
            "scenario": ["RCP45"],
        },
    )

    f.set_target(obs)

    mock_scm._run_single.return_value = scm_out

    e = f._get_evaluator(mock_scm)
    with pytest.raises(ValueError):
        e()


def get_finder(param_set, target, metric_func, reference_period):
    finder = PointEstimateFinder(param_set)
    finder.target = target
    finder.parameter_set = param_set
    finder.metric_func = metric_func
    finder.reference_period = reference_period

    return finder


def test_evaluator_simple(mock_scm):
    ps = ParameterSet()
    ps.set_tune("test", Normal())

    mock_scm._run_single.return_value = create_iam_dataframe(
        np.arange(11, 21), index=range(2000, 2010)
    )

    resulting_metric_value = 1.0
    metric_func = Mock()
    metric_func.return_value = resulting_metric_value

    target = prepare_dataframe(
        create_iam_dataframe(pd.Series(range(20, 30), index=range(2000, 2010)))
    )

    finder = get_finder(ps, target, metric_func, (datetime(2000, 1, 1),))
    e = Evaluator(finder, mock_scm)

    res = e(test=3)

    assert mock_scm._run_single.call_args[0] == ({"test": 3},)
    np.testing.assert_array_almost_equal(
        metric_func.call_args[0], (np.arange(10), np.arange(20, 30))
    )
    assert res == resulting_metric_value

    # Check the timeseries and samples
    samples = e.samples
    assert len(samples) == 1
    assert samples.iloc[0].metric_value == 1.0
    assert samples.iloc[0].test == 3
    np.testing.assert_array_almost_equal(e.scm_df.values[0], np.arange(10))


def test_evaluator_reference_period(mock_scm):
    resulting_metric_value = 0.5
    metric_func = Mock(return_value=resulting_metric_value)

    scm_data = np.arange(11, 21)
    mock_scm._run_single.return_value = create_iam_dataframe(
        scm_data, index=range(2000, 2010)
    )

    finder = PointEstimateFinder(
        ParameterSet(), reference_period=(datetime(2000, 1, 1), datetime(2005, 12, 31))
    )
    finder.metric_func = metric_func
    target_data = np.arange(20, 30) ** 2
    finder.set_target(
        create_iam_dataframe(
            pd.Series(target_data, index=range(2000, 2010)),
            variable="Surface Temperature",
        )
    )
    e = Evaluator(finder, mock_scm)

    e()

    np.testing.assert_array_almost_equal(
        metric_func.call_args[0][0], scm_data - scm_data[:6].mean()
    )
    np.testing.assert_array_almost_equal(
        metric_func.call_args[0][1], target_data - target_data[:6].mean()
    )


def test_evaluator_metric_call(point_finder, target_ts, mock_scm):
    point_finder.set_target(
        target_ts.filter(variable="Surface Temperature"),
        weights={"Surface Temperature": 10.0},
    )

    target_vals = np.arange(0, 20, 2)
    np.testing.assert_array_equal(point_finder.target.values.squeeze(), target_vals)

    # Don't actually run the model, but mock the result to be 1.2 * the target
    mock_results = 1.2 * target_vals
    e = point_finder._get_evaluator(mock_scm)
    res = deepcopy(target_ts)
    res._data *= 1.2
    mock_scm._run_single.return_value = res
    e.metric_func = Mock(return_value=1)

    # Call the (mocked) scm with no additional config
    e()

    e.metric_func.assert_called_once()
    args, kwargs = e.metric_func.call_args
    np.testing.assert_array_almost_equal(args[0], mock_results)
    np.testing.assert_array_almost_equal(args[1], target_vals)
    np.testing.assert_array_almost_equal(kwargs["sample_weight"], [10.0] * 10)


def test_evaluator_no_weights(point_finder, target_ts, mock_scm):
    point_finder.set_target(target_ts.filter(variable="Surface Temperature"))

    target_vals = np.arange(0, 20, 2)
    np.testing.assert_array_equal(point_finder.target.values.squeeze(), target_vals)

    # Don't actually run the model, but mock the result to be 1.2 * the target
    e = Evaluator(point_finder, mock_scm)
    res = deepcopy(target_ts)
    res._data *= 1.2
    mock_scm._run_single.return_value = res
    e.metric_func = Mock(return_value=1.0)

    # Call the (mocked) scm with no additional config
    e()

    e.metric_func.assert_called_once()
    args, kwargs = e.metric_func.call_args
    np.testing.assert_array_almost_equal(args[0], target_vals * 1.2)
    np.testing.assert_array_almost_equal(args[1], target_vals)
    assert kwargs["sample_weight"] is None


def test_target_no_weights(point_finder, target_ts):
    point_finder.set_target(target_ts)

    assert point_finder.weights is None


def test_target_weights_dict(point_finder, target_ts):
    point_finder.set_target(target_ts, weights={"Surface Temperature": 10.0})

    obs = point_finder.weights[("Surface Temperature", "World")]
    exp = np.ones(shape=(10,)) * 10.0
    np.testing.assert_array_equal(obs, exp)

    obs = point_finder.weights[("SLR_EXPANSION", "World")]
    exp = np.ones(shape=(10,))
    np.testing.assert_array_equal(obs, exp)


def test_target_weights_df(point_finder, target_ts):
    weights = deepcopy(target_ts)
    weights._data.loc[:, :] = 1.0
    weights._data.loc[:5, 0] = 5.0
    weights._data.loc[5:, 0] = 10.0

    point_finder.set_target(target_ts, weights=weights)

    obs = point_finder.weights[("Surface Temperature", "World")]
    exp = np.array([5.0] * 5 + [10.0] * 5)
    np.testing.assert_array_equal(obs, exp)

    obs = point_finder.weights[("SLR_EXPANSION", "World")]
    exp = np.ones(shape=(10,))
    np.testing.assert_array_equal(obs, exp)


def test_target_weights_subset(point_finder, target_ts):
    # weights is a subset of target_ts
    weights = target_ts.filter(variable="SLR_EXPANSION")
    weights._data.loc[:5] = 5.0
    weights._data.loc[5:] = 10.0

    point_finder.set_target(target_ts, weights=weights)

    # variables not in `weights` should have weights of 1.0
    obs = point_finder.weights[("Surface Temperature", "World")]
    exp = np.ones(shape=(10,))
    np.testing.assert_array_equal(obs, exp)

    obs = point_finder.weights[("SLR_EXPANSION", "World")]
    exp = np.array([5.0] * 5 + [10.0] * 5)
    np.testing.assert_array_equal(obs, exp)


def test_target_weights_dict_temporal(point_finder, target_ts):
    point_finder.set_target(
        target_ts, weights={"Surface Temperature": [5.0] * 5 + [10.0] * 5}
    )

    obs = point_finder.weights[("Surface Temperature", "World")]
    exp = np.array([5.0] * 5 + [10.0] * 5)
    np.testing.assert_array_equal(obs, exp)

    obs = point_finder.weights[("SLR_EXPANSION", "World")]
    exp = np.ones(shape=(10,))
    np.testing.assert_array_equal(obs, exp)


def test_target_weights_empty_filter(point_finder, target_ts):
    pytest.raises(
        ValueError, point_finder.set_target, target_ts, weights={"Random Variable": 3}
    )


def test_target_weights_filter_syntax(point_finder, target_ts):
    target_ts.set_meta(name="variable", meta=["Ocean Layer|1", "Ocean Layer|2"])

    point_finder.set_target(target_ts, weights={"Ocean Layer|*": 2.0})

    obs = point_finder.weights[("Ocean Layer|1", "World")]
    exp = np.array([2.0] * 10)
    np.testing.assert_array_equal(obs, exp)

    obs = point_finder.weights[("Ocean Layer|2", "World")]
    exp = np.array([2.0] * 10)
    np.testing.assert_array_equal(obs, exp)


@pytest.mark.parametrize("inp", (np.nan, np.inf, -np.inf, None))
def test_evaluator_nonfinite_result(caplog, point_finder, target_ts, mock_scm, inp):
    point_finder.set_target(target_ts.filter(variable="Surface Temperature"))

    target_vals = np.arange(0, 20, 2)
    np.testing.assert_array_equal(point_finder.target.values.squeeze(), target_vals)

    # Don't actually run the model, but mock the result to be 1.2 * the target
    e = Evaluator(point_finder, mock_scm)
    res = deepcopy(target_ts)
    res._data *= 1.2
    mock_scm._run_single.return_value = res

    if inp is not None:
        e.metric_func = Mock(return_value=inp)
    else:
        e.metric_func = Mock(side_effect=ValueError)

    # Call the (mocked) scm with no additional config
    res = e()

    assert res == LARGE_NEG_ERR

    # Check that a warning was logged
    if inp is None:
        assert caplog.records[0].message.startswith(
            "Could not calculate metric value for run "
        )
    assert caplog.records[-1].levelname == "WARNING"
    assert (
        caplog.records[-1].message
        == "Non finite metric value found. using LARGE_NEG_ERR (-1e+09)"
    )


@pytest.mark.parametrize("inp", ({}, {"test": 1}))
def test_evaluator_metrics(point_finder, target_ts, mock_scm, inp):
    point_finder.set_target(target_ts.filter(variable="Surface Temperature"))

    target_vals = np.arange(0, 20, 2)
    np.testing.assert_array_equal(point_finder.target.values.squeeze(), target_vals)

    # Don't actually run the model, but mock the result to be 1.2 * the target
    e = Evaluator(point_finder, mock_scm)
    res = deepcopy(target_ts)
    res._data *= 1.2
    mock_scm._run_single.return_value = res

    res = e.metrics(**inp)

    assert mock_scm._run_single.called_with(inp)

    for k in metrics:
        assert k in res


@pytest.mark.parametrize(
    "lower,value", ((0, 1000), (-100, 1000), (100, 1000), (0, 10000))
)
def test_transforms(point_finder, mock_scm, target_ts, lower, value):
    point_finder.apply_transform("penalise(lower={}, value={})".format(lower, value))
    point_finder.set_target(target_ts.filter(variable="Surface Temperature"))

    e = Evaluator(point_finder, mock_scm)
    res = deepcopy(target_ts)
    res._data[:] = res._data[:] - 10
    mock_scm._run_single.return_value = res

    e()

    # check that the right values have been masked
    ts = e._timeseries[0]
    mask = res._data.values < lower
    np.testing.assert_allclose(ts.values[mask], value)
    assert (ts.values[~mask] < value).all()


def test_transforms_filters(point_finder, mock_scm, target_ts):
    filters = {"variable": "Surface Temperature"}
    point_finder.apply_transform("penalise(lower=0)", filters=filters)
    point_finder.set_target(target_ts)

    e = Evaluator(point_finder, mock_scm)
    res = deepcopy(target_ts)
    res._data[:] = res._data[:] - 10
    mock_scm._run_single.return_value = res

    e()

    ts = e._timeseries[0]
    ts_1 = ts.T[ts.columns.get_level_values("variable") == "Surface Temperature"].T
    mask = res.filter(**filters)._data.values < 0
    np.testing.assert_allclose(ts_1.values[mask], 1e9)

    ts_2 = ts.T[ts.columns.get_level_values("variable") != "Surface Temperature"].T
    assert (ts_2.values < 100).all()
