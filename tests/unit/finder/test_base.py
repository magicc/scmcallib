from datetime import datetime

import numpy as np
import pandas as pd
import pytest
from scmdata import ScmDataFrame

from scmcallib.finder.base import BaseFinder
from scmcallib.parameterset import ParameterSet


def test_ref_period_too_many():
    ps = ParameterSet()

    pytest.raises(ValueError, BaseFinder, ps, reference_period=(2000, 2005, 2010))


def test_reference_period_single():
    ps = ParameterSet()

    f = BaseFinder(ps, reference_period=(datetime(2000, 6, 12),))
    assert f.reference_period == (datetime(2000, 6, 12), datetime(2000, 6, 12))

    f = BaseFinder(ps, reference_period=2000)
    assert f.reference_period == (datetime(2000, 1, 1), datetime(2000, 12, 31))


def test_reference_period_start_end_int():
    ps = ParameterSet()

    f = BaseFinder(ps, reference_period=(2000, 2010))
    assert f.reference_period == (datetime(2000, 1, 1), datetime(2010, 12, 31))


def test_reference_period_start_end():
    ps = ParameterSet()

    f = BaseFinder(ps, reference_period=(datetime(2000, 6, 12), datetime(2010, 12, 1)))
    assert f.reference_period == (datetime(2000, 6, 12), datetime(2010, 12, 1))


def test_reference_period_start_end_str():
    ps = ParameterSet()

    f = BaseFinder(ps, reference_period=("2000", "2010-03-12"))
    assert f.reference_period == (datetime(2000, 1, 1), datetime(2010, 3, 12))


def test_target():
    f = BaseFinder(ParameterSet())

    obs = ScmDataFrame(
        pd.DataFrame(np.arange(20), index=range(2000, 2020)),
        columns={
            "unit": ["K"],
            "variable": ["Surface Temperature"],
            "region": ["World"],
            "model": ["unspecified"],
            "scenario": ["RCP45"],
        },
    )

    f.set_target(obs)

    assert f.target[:11].mean().squeeze() == 0.0


def test_target_reference_period():
    f = BaseFinder(ParameterSet(), reference_period=2002)

    obs = ScmDataFrame(
        pd.DataFrame(np.arange(20), index=range(2000, 2020)),
        columns={
            "unit": ["K"],
            "variable": ["Surface Temperature"],
            "region": ["World"],
            "model": ["unspecified"],
            "scenario": ["RCP45"],
        },
    )

    f.set_target(obs)

    assert f.target.iloc[0].values == -2.0
    assert f.target.iloc[2].values == 0.0


def test_target_iter():
    f = BaseFinder(ParameterSet())

    obs = ScmDataFrame(
        pd.DataFrame(np.arange(10).reshape((5, 2)), index=range(2000, 2005)),
        columns={
            "unit": ["K"],
            "variable": ["Surface Temperature"],
            "region": ["World"],
            "model": ["unspecified"],
            "scenario": ["RCP45", "RCP85"],
        },
    )

    f.set_target(obs, iter_over="scenario")

    np.testing.assert_array_equal(
        f.iter_over_values, [{"scenario": "RCP45"}, {"scenario": "RCP85"}]
    )


def test_target_duplicates():
    f = BaseFinder(ParameterSet())

    # There are two timeseries with the same model/region
    obs = ScmDataFrame(
        pd.DataFrame(np.arange(10).reshape((5, 2)), index=range(2000, 2005)),
        columns={
            "unit": ["K"],
            "variable": ["Surface Temperature"],
            "region": ["World"],
            "model": ["unspecified"],
            "scenario": ["RCP45", "RCP85"],
        },
    )

    pytest.raises(ValueError, f.set_target, obs)


def test_target_nan():
    f = BaseFinder(ParameterSet())

    # There are two timeseries with the same model/region
    data = np.arange(10).reshape((5, 2)).astype(float)
    data[4, 0] = np.nan
    obs = ScmDataFrame(
        pd.DataFrame(data, index=range(2000, 2005)),
        columns={
            "unit": ["K"],
            "variable": ["Surface Temperature"],
            "region": ["World", "Other"],
            "model": ["unspecified"],
            "scenario": ["RCP45", "RCP85"],
        },
    )

    f.set_target(obs, drop_na=True)
    pd.testing.assert_index_equal(
        f.target.index,
        pd.Index(
            [
                datetime(2000, 1, 1),
                datetime(2001, 1, 1),
                datetime(2002, 1, 1),
                datetime(2003, 1, 1),
            ],
            name="time",
            dtype=object,
        ),
    )
