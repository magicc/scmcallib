import pytest

from scmcallib import ParameterSet


def test_distribution_iter_over(mock_scm, DistributionFinder):
    df = DistributionFinder(ParameterSet(), scm=mock_scm)
    pytest.raises(NotImplementedError, df.set_target, [], iter_over="scenario")
    pytest.raises(
        NotImplementedError, df.set_target, [], df_kwargs={"iter_over": "scenario"}
    )
