import numpy as np
import pandas as pd
import pytest

from scmcallib.transforms import _parse_transform, get_transform, penalise, diff


@pytest.mark.parametrize(
    "inp,exp",
    (
        ("penalise", ("penalise", [], {})),
        ("penalise()", ("penalise", [], {})),
        ("penalise(1)", ("penalise", [1], {})),
        ("penalise(-1)", ("penalise", [-1], {})),
        ("penalise(-2.5)", ("penalise", [-2.5], {})),
        ('penalise("test_str")', ("penalise", ["test_str"], {})),
        ('penalise((1, 2.0, "test"))', ("penalise", [(1, 2.0, "test")], {})),
        ('penalise([1, 2.0, "test"])', ("penalise", [(1, 2.0, "test")], {})),
        ("penalise(lower=1)", ("penalise", [], {"lower": 1})),
        ("penalise(lower=1, upper=2)", ("penalise", [], {"lower": 1, "upper": 2})),
        ("penalise(lower=1,upper=2)", ("penalise", [], {"lower": 1, "upper": 2})),
        ("penalise(1,upper=2)", ("penalise", [1], {"upper": 2})),
        ("other(1,upper=2)", ("other", [1], {"upper": 2})),
    ),
)
def test_parse_transform(inp, exp):
    res = _parse_transform(inp)

    assert res == exp


@pytest.mark.parametrize(
    "inp", ("penalise(test=1,2)", "penalise(variable)", "penalise(", "penalise)")
)
def test_parse_transform_invalid(inp):
    with pytest.raises(SyntaxError):
        _parse_transform(inp)


def test_get_transform_unknown():
    with pytest.raises(ValueError, match="Unknown transform: testing"):
        get_transform("testing")


def test_get_transform_both_args():
    with pytest.raises(ValueError, match=r"Can't provide args"):
        get_transform("penalise(2)", 1)

    with pytest.raises(ValueError, match=r"Can't provide kwargs"):
        get_transform("penalise(test=1)", kw=1)


@pytest.mark.parametrize(
    'lower,upper,value',
    (
        (4, None, 1e7),
        (4, None, 1e8),
        (5, 8, 1e8),
        (None, 8, 1e8),
    )
)
def test_penalise(target_ts, lower, upper, value):
    df = target_ts.timeseries()
    exp = df.copy()
    exp.values[exp < lower] = value
    exp.values[exp > upper] = value

    # Check that they are different
    assert not (exp == df).all(axis=None)

    p = penalise(lower=lower, upper=upper, value=value)

    res = p(df)

    pd.testing.assert_frame_equal(exp, res)


def test_diff(target_ts):
    df = target_ts.timeseries()
    exp = df.copy()
    exp.values[0, 1:] = np.diff(exp.values[0])
    exp.values[0, 0] = np.nan

    p = diff()

    res = p(df)

    np.testing.assert_allclose(exp.values, res.values)
    assert res.index.get_level_values(level="unit")[0] == exp.index.get_level_values(level="unit")[0] + " / yr"


def test_diff_non_annual(target_ts):
    df = target_ts.resample("MS").timeseries()

    p = diff()

    with pytest.raises(AssertionError):
        p(df)
