from os.path import join
from shutil import copyfile
from tempfile import TemporaryDirectory

import numpy as np
import pandas as pd
import pytest
from conftest import TEST_DATA_DIR
from scmcallib.distributions import Uniform
from scmcallib.tuningcore import (
    read_csv_tuningcore,
    create_point_model,
    get_target,
    _handle_simcap_varnames,
    _maybe_as_float
)

tuning_data = {
    "RCP85_C0": [
        {
            "file_variable": "npp",
            "region": "World",
            "file": "netcdf-scm_npp_Lmon_CanESM5_hist-nat_r8i1p1f1_gn_185001-202012.MAG",
            "weight": 1078.0,
        }
    ],
    "RCP85_C1": [
        {
            "file_variable": "npp",
            "region": "World|Northern Hemisphere",
            "file": "netcdf-scm_npp_Lmon_CanESM5_hist-nat_r8i1p1f1_gn_185001-202012.MAG",
            "weight": 10.9,
        }
    ],
}


def test_handle_varnames_carboncycle():
    fname = "npp_WITHIN_CABLE_RCP85-C0.csv"
    d = pd.Series(
        {
            "name": "CARBONCYCLE",
            "scenario": "RCP85_C0",
            "colcode": "CURRENT_NPP",
            "purpose": "DATSOURCE",
            "Cable2018": fname,
        }
    )

    exp = {
        "variable": "CARBONCYCLE_CURRENT_NPP",
        "region": "World",
        "file": "CABLE_RCP85-C0.csv",
        "file_variable": "npp",
    }
    res = _handle_simcap_varnames(d, fname)

    assert res == exp


def test_handle_varnames_dat():
    fname = "npp_WITHIN_CABLE_RCP85-C0.csv"
    d = pd.Series(
        {
            "name": "DAT_SURF_TEMPERATURE",
            "scenario": "RCP85_C0",
            "colcode": "BOXGLOBAL",
            "purpose": "DATSOURCE",
            "Cable2018": fname,
        }
    )

    exp = {
        "variable": "DAT_SURF_TEMPERATURE",
        "region": "World",
        "file": "CABLE_RCP85-C0.csv",
        "file_variable": "npp",
    }
    res = _handle_simcap_varnames(d, fname)

    assert res == exp


def test_handle_varnames_no_within():
    fname = "CABLE_RCP85-C0.csv"
    d = pd.Series(
        {
            "name": "DAT_SURF_TEMPERATURE",
            "scenario": "RCP85_C0",
            "colcode": "BOXGLOBAL",
            "purpose": "DATSOURCE",
            "Cable2018": fname,
        }
    )

    exp = {
        "variable": "DAT_SURF_TEMPERATURE",
        "region": "World",
        "file": "CABLE_RCP85-C0.csv",
        "file_variable": "DAT_SURF_TEMPERATURE",
    }
    res = _handle_simcap_varnames(d, fname)

    assert res == exp


def test_can_read_csv_with_csv():
    fname = join(TEST_DATA_DIR, "tuningcore_CMIP6_CarbonCycle_22Oct2019_clean.csv")
    res = read_csv_tuningcore(fname, data_dir=TEST_DATA_DIR, scensets_fname="SCENSET_20191108T165316.csv")

    assert len(res["runs"]) == 1
    m = res["runs"][0]

    assert m["code"] == "IPSL-CM6A-LR"
    assert m["name"] == "r1i1p1f1"

    assert len(m["fixed_parameters"]) == 3
    assert len(m["free_parameters"]) == 16

    assert len(m["scenarios"]) == 5
    sce_1 = m["scenarios"][0]
    assert sce_1["name"] == "HISTORICAL-TAS"
    exp = {
        'BCOC_SWITCHFROMRF2EMIS_YEAR': 2000,
        'CH4_SWITCHFROMCONC2EMIS_YEAR': 10000,
        'CO2_PREINDCO2CONC': 278,
        'CO2_PREINDCO2CONC_APPLY': 0,
        'CO2_SWITCHFROMCONC2EMIS_YEAR': 10000,
        'CORE_AMV_APPLY': 0,
        'CORE_ELNINO_APPLY': 0,
        'ENDYEAR': 2500,
        'FGAS_FILES_CONC': [
            'HISTORICAL_CF4_CONC.IN', 'HISTORICAL_C2F6_CONC.IN', 'HISTORICAL_C3F8_CONC.IN', 'HISTORICAL_C4F10_CONC.IN',
            'HISTORICAL_C5F12_CONC.IN', 'HISTORICAL_C6F14_CONC.IN', 'HISTORICAL_C7F16_CONC.IN', 'HISTORICAL_C8F18_CONC.IN',
            'HISTORICAL_CC4F8_CONC.IN', 'HISTORICAL_HFC23_CONC.IN', 'HISTORICAL_HFC32_CONC.IN', 'HISTORICAL_HFC4310_CONC.IN',
            'HISTORICAL_HFC125_CONC.IN', 'HISTORICAL_HFC134A_CONC.IN', 'HISTORICAL_HFC143A_CONC.IN', 'HISTORICAL_HFC152A_CONC.IN',
            'HISTORICAL_HFC227EA_CONC.IN', 'HISTORICAL_HFC236FA_CONC.IN', 'HISTORICAL_HFC245FA_CONC.IN',
            'HISTORICAL_HFC365MFC_CONC.IN', 'HISTORICAL_NF3_CONC.IN', 'HISTORICAL_SF6_CONC.IN', 'HISTORICAL_SO2F2_CONC.IN'
        ],
        'FGAS_FILES_EMIS': [
            'HISTSSP_CF4_EMIS.IN', 'HISTSSP_C2F6_EMIS.IN', 'HISTSSP_C3F8_EMIS.IN', 'HISTSSP_C4F10_EMIS.IN',
            'HISTSSP_C5F12_EMIS.IN', 'HISTSSP_C6F14_EMIS.IN', 'HISTSSP_C7F16_EMIS.IN', 'HISTSSP_C8F18_EMIS.IN',
            'HISTSSP_CC4F8_EMIS.IN', 'HISTSSP_HFC23_EMIS.IN', 'HISTSSP_HFC32_EMIS.IN', 'HISTSSP_HFC4310_EMIS.IN',
            'HISTSSP_HFC125_EMIS.IN', 'HISTSSP_HFC134A_EMIS.IN', 'HISTSSP_HFC143A_EMIS.IN', 'HISTSSP_HFC152A_EMIS.IN',
            'HISTSSP_HFC227EA_EMIS.IN', 'HISTSSP_HFC236FA_EMIS.IN', 'HISTSSP_HFC245FA_EMIS.IN', 'HISTSSP_HFC365MFC_EMIS.IN',
            'HISTSSP_NF3_EMIS.IN', 'HISTSSP_SF6_EMIS.IN', 'HISTSSP_SO2F2_EMIS.IN'
        ],
        'FGAS_SWITCHFROMCONC2EMIS_YEAR': 10000,
        'FILE_BCB_EMIS': 'HISTORICAL_BCB_EMIS.IN',
        'FILE_BCB_OT': 'GISS_BCB_OT.IN',
        'FILE_BCB_RF': 'GISS_BCB_RF.IN',
        'FILE_BCI_EMIS': 'HISTORICAL_BCI_EMIS.IN',
        'FILE_BCI_OT': 'GISS_BCI_OT.IN',
        'FILE_BCI_RF': 'GISS_BCI_RF.IN',
        'FILE_BCSNOW_RF': 'GISS_BCSNOW_RF.IN',
        'FILE_CH4B_EMIS': 'HISTSSP_CH4B_EMIS.IN',
        'FILE_CH4I_EMIS': 'HISTSSP_CH4I_EMIS.IN',
        'FILE_CH4_CONC': 'HISTORICAL_CH4_CONC.IN',
        'FILE_CO2B_EMIS': 'HISTSSP_CO2B_EMIS.IN',
        'FILE_CO2I_EMIS': 'HISTSSP_CO2I_EMIS.IN',
        'FILE_CO2_CONC': 'HISTORICAL_CO2_CONC.IN',
        'FILE_COB_EMIS': 'HISTORICAL_COB_EMIS.IN',
        'FILE_COI_EMIS': 'HISTORICAL_COI_EMIS.IN',
        'FILE_EMISSCEN': '',
        'FILE_EMISSCEN_2': '',
        'FILE_EMISSCEN_3': '',
        'FILE_EMISSCEN_4': '',
        'FILE_EMISSCEN_5': '',
        'FILE_EMISSCEN_6': '',
        'FILE_EMISSCEN_7': '',
        'FILE_EMISSCEN_8': '',
        'FILE_LANDUSE_RF': 'GISS_LANDUSE_RF.IN',
        'FILE_MINERALDUST_RF': 'GISS_LANDUSE_RF.IN',
        'FILE_N2OB_EMIS': 'HISTSSP_N2OB_EMIS.IN',
        'FILE_N2OI_EMIS': 'HISTSSP_N2OI_EMIS.IN',
        'FILE_N2O_CONC': 'HISTORICAL_N2O_CONC.IN',
        'FILE_NH3B_EMIS': 'HISTORICAL_NH3B_EMIS.IN',
        'FILE_NH3I_EMIS': 'HISTORICAL_NH3I_EMIS.IN',
        'FILE_NH3T_RF': '',
        'FILE_NMVOCB_EMIS': 'HISTORICAL_NMVOCB_EMIS.IN',
        'FILE_NMVOCI_EMIS': 'HISTORICAL_NMVOCI_EMIS.IN',
        'FILE_NOXB_EMIS': 'HISTORICAL_NOXB_EMIS.IN',
        'FILE_NOXB_OT': 'MIXED_NOXB_OT.IN',
        'FILE_NOXI_EMIS': 'HISTORICAL_NOXI_EMIS.IN',
        'FILE_NOXI_OT': 'MIXED_NOXI_OT.IN',
        'FILE_NOXT_RF': 'GISS_NOX_RF.IN',
        'FILE_OCB_EMIS': 'HISTORICAL_OCB_EMIS.IN',
        'FILE_OCB_OT': 'GISS_OCB_OT.IN',
        'FILE_OCB_RF': 'GISS_OCB_RF.IN',
        'FILE_OCI_EMIS': 'HISTORICAL_OCI_EMIS.IN',
        'FILE_OCI_OT': 'GISS_OCI_OT.IN',
        'FILE_OCI_RF': 'GISS_OCI_RF.IN',
        'FILE_OCN_OT': 'GISS_OCN_OT.IN',
        'FILE_PRESCR_SURFACETEMP': 'NORMED_TAS_HISTORICAL_IPSL-CM6A-LR_R1I1P1F1_FOURBOX_SURFACE_TEMP.IN',
        'FILE_SOLAR_RF': 'HISTORICAL_SOLAR_RF.IN',
        'FILE_SOXB_EMIS': 'HISTORICAL_SOXB_EMIS.IN',
        'FILE_SOXI_EMIS': 'HISTORICAL_SOXI_EMIS.IN',
        'FILE_SOXI_OT': 'GISS_SOXI_OT.IN',
        'FILE_SOXNB_OT': 'GISS_SOXNB_OT.IN',
        'FILE_SOXT_RF': 'GISS_SOX_RF.IN',
        'FILE_VOLCANIC_RF': 'HISTORICAL_VOLCANIC_RF.MON',
        'MHALO_FILES_CONC': [
            'HISTORICAL_CFC11_CONC.IN', 'HISTORICAL_CFC12_CONC.IN', 'HISTORICAL_CFC113_CONC.IN', 'HISTORICAL_CFC114_CONC.IN',
            'HISTORICAL_CFC115_CONC.IN', 'HISTORICAL_HCFC22_CONC.IN', 'HISTORICAL_HCFC141B_CONC.IN',
            'HISTORICAL_HCFC142B_CONC.IN', 'HISTORICAL_CH3CCL3_CONC.IN', 'HISTORICAL_CCL4_CONC.IN',
            'HISTORICAL_CH3CL_CONC.IN', 'HISTORICAL_CH2CL2_CONC.IN', 'HISTORICAL_CHCL3_CONC.IN',
            'HISTORICAL_CH3BR_CONC.IN', 'HISTORICAL_HALON1211_CONC.IN', 'HISTORICAL_HALON1301_CONC.IN',
            'HISTORICAL_HALON2402_CONC.IN', ''
        ],
        'MHALO_FILES_EMIS': [
            'HISTSSP_CFC11_EMIS.IN', 'HISTSSP_CFC12_EMIS.IN', 'HISTSSP_CFC113_EMIS.IN', 'HISTSSP_CFC114_EMIS.IN',
            'HISTSSP_CFC115_EMIS.IN', 'HISTSSP_HCFC22_EMIS.IN', 'HISTSSP_HCFC141B_EMIS.IN', 'HISTSSP_HCFC142B_EMIS.IN',
            'HISTSSP_CH3CCL3_EMIS.IN', 'HISTSSP_CCL4_EMIS.IN', 'HISTSSP_CH3CL_EMIS.IN', 'HISTSSP_CH2CL2_EMIS.IN',
            'HISTSSP_CHCL3_EMIS.IN', 'HISTSSP_CH3BR_EMIS.IN', 'HISTSSP_HALON1211_EMIS.IN', 'HISTSSP_HALON1301_EMIS.IN',
            'HISTSSP_HALON2402_EMIS.IN', 'HISTSSP_HALON1202_EMIS.IN'
        ],
        'MHALO_SWITCHFROMCONC2EMIS_YEAR': 10000,
        'N2O_SWITCHFROMCONC2EMIS_YEAR': 10000,
        'RF_INITIALIZATION_METHOD': 'ZEROSTARTSHIFT',
        'RF_TOTAL_RUNMODUS': 'ALL',
        'SCEN_CO2BHISTADJUST_0NO1SCALE2SHIFT': 0,
        'SCEN_HISTADJUST_0NO1SCALE2SHIFT': 0,
        'STARTYEAR': 1850
    }
    assert sce_1["parameters"] == exp


def test_can_read_csv_with_scensets():
    with TemporaryDirectory() as td:
        copyfile(
            join(TEST_DATA_DIR, "tuningcore_example.csv"),
            join(td, "tuningcore_example.csv"),
        )
        copyfile(join(TEST_DATA_DIR, "SCENSETS.mat"), join(td, "SCENSETS_custom.mat"))

        res = read_csv_tuningcore(
            join(td, "tuningcore_example.csv"),
            data_dir=td,
            scensets_fname="SCENSETS_custom.mat",
        )

        assert len(res["runs"]) == 1
        m = res["runs"][0]

        data = {d["name"]: d["parameters"] for d in m["scenarios"]}
        assert data == {
            "RCP85_C0": {"RUNNAME": "TUNING_RCP85", "FILE_EMISSCEN": "RCP85.SCEN7"},
            "RCP85_C1": {"RUNNAME": "TUNING_RCP85", "FILE_EMISSCEN": "RCP85.SCEN7"},
        }


def test_can_read_csv_with_data_dir():
    with TemporaryDirectory() as td:
        copyfile(join(TEST_DATA_DIR, "SCENSETS.mat"), join(td, "SCENSETS.mat"))

        res = read_csv_tuningcore(
            join(TEST_DATA_DIR, "tuningcore_example.csv"), data_dir=td, scensets_fname="SCENSETS.mat"
        )

        assert len(res["runs"]) == 1
        m = res["runs"][0]

        data = {d["name"]: d["parameters"] for d in m["scenarios"]}
        assert data == {
            "RCP85_C0": {"RUNNAME": "TUNING_RCP85", "FILE_EMISSCEN": "RCP85.SCEN7"},
            "RCP85_C1": {"RUNNAME": "TUNING_RCP85", "FILE_EMISSCEN": "RCP85.SCEN7"},
        }


@pytest.mark.parametrize('data_dir', (None, TEST_DATA_DIR))
def test_create_point_model(tuningcore, data_dir):
    m = tuningcore["runs"][0]

    # Overwrite the tuning data with data which is present in the repo
    for sce in m["scenarios"]:
        sce["tuning_data"] = tuning_data[sce["name"]]

    if data_dir is None:
        # Append the TEST_DATA_DIR to the filenames
        for sce in m["scenarios"]:
            for d in sce["tuning_data"]:
                d["file"] = join(TEST_DATA_DIR, d["file"])

    # reference period is needed
    point = create_point_model(m, reference_period=(1850, 1855), data_dir=data_dir)

    params = point.parameter_set

    assert list(params.tune_parameters.keys()) == ["CO2_FEEDBACKFACTOR_NPP"]
    d = params.tune_parameters["CO2_FEEDBACKFACTOR_NPP"]
    assert isinstance(d, Uniform)
    assert d.lower == -0.2
    assert d.upper == 0.2

    params.state = {"scenario": "RCP85_C0"}
    assert dict(params.config_parameters) == {
        "RUNNAME": "TUNING_RCP85",
        "FILE_EMISSCEN": "WMO_MHALO.SCEN7",
        "FILE_TUNINGMODEL_1": "C4MIP_UVIC",
        "FILE_TUNINGMODEL_2": "FULLTUNE_MEDIUM_CMIP3_ECS2_5",
        "GHGAER_SCEN_SCENADJUST2HIST_0NO1SCALE": 1,
        "FILE_EMISSCEN_3": "RCP85_CO2B_NULL.SCEN7",
        "FILE_EMISSCEN_2": "Velders_HFC_Kigali.SCEN7",
        "FILE_CO2B_EMIS": "HISTNULL_CO2B_EMIS.IN",
        "FILE_PRESCR_SURFACETEMP": "CABLE_CALIBRATION_PRESCTEMP_RCP85.IN",
        "CORE_PRESCRTEMP_APPLY": 1,
        "CORE_SWITCHFROMPRESC2TEMP_YEAR": 2110,
        "CO2_SWITCHFROMCONC2EMIS_YEAR": 2110,
        "FILE_CO2_CONC": "RCP85_ENDOFYEAR_CO2_CONC_CABLEC0.IN",
        "CO2_FEEDTEMP_LANDONLY_APPLY": 1,
        "CO2_TEMPFEEDBACK_YRSTART": 1760,
        "CO2_FERTILIZATION_YRSTART": 1760,
        "NCYCLE_APPLY": 0,
        "CO2_FERTILIZATION_METHOD": 3,
        "CO2_FERTILIZATION_FACTOR": 1.0,
        "NCYCLE_TURNOVERTIME_LOSSES": 2.0,
    }

    assert point.target is not None
    assert point.iter_over_values == [
        {"scenario": "RCP85_C0"},
        {"scenario": "RCP85_C1"},
    ]


@pytest.mark.parametrize('data_dir', (None, TEST_DATA_DIR))
def test_get_target_mag(data_dir):
    td = tuning_data.copy()
    if data_dir is None:
        # Append the TEST_DATA_DIR to the filenames
        for sce in td:
            for d in td[sce]:
                d["file"] = join(TEST_DATA_DIR, d["file"])

    df, weights = get_target(td, data_dir=data_dir)

    assert np.isclose(
        df.filter(scenario="RCP85_C0").timeseries().iloc[0, 0], 9.36527e-09
    )
    assert np.isclose(
        df.filter(scenario="RCP85_C1").timeseries().iloc[0, 0], 4.85691e-09
    )


@pytest.mark.parametrize(
    "data",
    [
        {
            "RCP85-C0": [
                {
                    "file_variable": "npp",
                    "region": "World",
                    "file": join(TEST_DATA_DIR, "CABLE_RCP85-C0.csv"),
                    "weight": 1078.0,
                }
            ],
            "RCP85-C1": [
                {
                    "file_variable": "npp",
                    "region": "World",
                    "file": join(TEST_DATA_DIR, "CABLE_RCP85-C1.csv"),
                    "weight": 10.9,
                }
            ],
        },
        {
            "RCP85-C0": [
                {
                    "file_variable": "npp",
                    "region": "World",
                    "file": join(TEST_DATA_DIR, "CABLE_merged.csv"),
                    "weight": 1078.0,
                }
            ],
            "RCP85-C1": [
                {
                    "file_variable": "npp",
                    "region": "World",
                    "file": join(TEST_DATA_DIR, "CABLE_merged.csv"),
                    "weight": 10.9,
                }
            ],
        },
    ],
)
def test_get_target_csv(data):
    df, weights = get_target(data)

    assert np.isclose(df.filter(scenario="RCP85-C0").values[0, 0], 55.17818182)
    assert np.isclose(df.filter(scenario="RCP85-C1").values[0, 0], 55.38871205)


@pytest.mark.parametrize(
    "data",
    [
        {
            "RCP85-C0": [
                {
                    "file_variable": "not_present",
                    "region": "World",
                    "file": join(TEST_DATA_DIR, "CABLE_RCP85-C0.csv"),
                    "weight": 1078.0,
                }
            ]
        },
        {
            "RCP85-C0": [
                {
                    "file_variable": "npp",
                    "region": "Invalid",
                    "file": join(TEST_DATA_DIR, "CABLE_RCP85-C0.csv"),
                    "weight": 1078.0,
                }
            ]
        },
        {
            "RCP85_missing": [
                {
                    "file_variable": "npp",
                    "region": "World",
                    "file": join(TEST_DATA_DIR, "CABLE_merged.csv"),
                    "weight": 1078.0,
                }
            ]
        },
    ],
)
def test_get_target_missing_data(data, caplog):
    # file contains a single sce
    with pytest.raises(ValueError, match="No objects to concatenate"):
        get_target(data)

    for sce in data:
        d = data[sce][0]
        assert "Could not find data for scenario {}, variable {} and region {} in {}".format(
            sce, d["file_variable"], d["region"], d["file"]
        )


def test_get_target_wrong_sce(caplog):
    # file contains a single sce
    fname = join(TEST_DATA_DIR, "CABLE_RCP85-C0.csv")
    df, weights = get_target(
        {
            "RCP85_missing": [
                {
                    "file_variable": "npp",
                    "region": "World",
                    "file": fname,
                    "weight": 1078.0,
                }
            ]
        }
    )
    assert len(df) == 1
    assert df["scenario"].unique() == ["RCP85_missing"]
    assert "Scenario RCP85_missing was not found in {}".format(fname) in caplog.text


def test_get_target_custom_variable():
    df, weights = get_target(
        {
            "RCP85-C0": [
                {
                    "variable": "CARBONCYCLE_GPP",
                    "file_variable": "npp",
                    "region": "World",
                    "file": join(TEST_DATA_DIR, "CABLE_RCP85-C0.csv"),
                    "weight": 1078.0,
                }
            ]
        }
    )
    assert len(df) == 1
    assert df["variable"].unique() == ["CARBONCYCLE_GPP"]


@pytest.mark.parametrize("v,exp", (
    ("A", "A"),
    ("100", 100),
    ("1000.0", 1000),
    ("0.74", 0.74),
    (0.74, 0.74),
    (1000000, 1000000),
    (1950, 1950),
    (1950.0, 1950),
    (1950.1, 1950.1),
    (["0.12", "A", "100", 200.4], [0.12, "A", 100, 200.4])
))
def test_maybe_as_float(v, exp):
    val = _maybe_as_float(v)
    if isinstance(val, (list, str)):
        assert val, exp
    else:
        np.testing.assert_almost_equal(val, exp)
        assert type(val) == type(exp)


def test_get_target_with_data(target_ts):
    data = target_ts.filter(variable="Surface Temperature")
    df, weights = get_target(
        {
            "RCP85": [
                {
                    "data": data,
                    "weight": 100
                }
            ]
        }
    )
    assert len(df) == 1
    assert df["scenario"].unique() == ["RCP85"]
    assert df["variable"].unique() == ["Surface Temperature"]
    assert (weights.values == 100).all()
    np.testing.assert_allclose(data.values, df.values)


def test_get_target_with_invalid_data(target_ts):
    with pytest.raises(AssertionError):
        get_target(
            {
                "RCP85": [
                    {
                        "data": target_ts, # Has 2 variables
                        "weight": 100
                    }
                ]
            }
        )
