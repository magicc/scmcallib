from unittest.mock import patch

import numpy as np
import pytest
from scmdata import ScmDataFrame

from scmcallib.utils import convert_scmdf_to_tuningstruc


@patch("scmcallib.utils.mat4py.savemat")
def test_convert_scmdf_to_tuningstruc_looping(mock_savemat):
    inp = ScmDataFrame(
        data=np.array([[1, 2, 3, 4], [4, 5, 6, 7], [7, 8, 9, 3], [-1, -2, -3, 2]]),
        columns={
            "climate_model": ["a_cm", "b_cm", "a_cm", "b_cm"],
            "scenario": ["a_scen", "a_scen", "b_scen", "b_scen"],
            "model": ["a_m", "b_m", "b_m", "a_m"],
            "variable": ["b_v", "a_v", "a_v", "b_v"],
            "region": ["b_r", "b_r", "a_r", "a_r"],
            "unit": ["b_u", "a_u", "a_u", "b_u"],
        },
        index=[1990, 2000, 2010, 2020],
    )

    convert_scmdf_to_tuningstruc(inp, "mocked_out")
    # not sure how to do assertion with df looping, this will do
    assert mock_savemat.call_count == 4


def test_convert_scmdf_to_tuningstruc_non_unique_timeseries_error():
    tid = "test"
    # impossible to make dataframe with duplicate rows so not an issue
    with pytest.raises(ValueError):
        inp = ScmDataFrame(
            data=np.array([[1, 2, 3], [4, 5, 6]]),
            columns={
                "climate_model": [tid],
                "scenario": [tid],
                "model": [tid],
                "variable": [tid],
                "region": [tid],
                "unit": [tid],
            },
            index=[1990, 2000],
        )

        inp.timeseries()
