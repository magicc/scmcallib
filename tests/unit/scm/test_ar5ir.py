from scmdata import ScmDataFrame

from scmcallib.scm import AR5IR


def test_ar5ir_output():
    scm = AR5IR()
    res = scm.run({})

    assert isinstance(res, ScmDataFrame)
