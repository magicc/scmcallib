from os import listdir
from unittest.mock import patch

import pytest
from pymagicc.core import WineNotInstalledError
from pymagicc.io import MAGICCData

from scmcallib.scm import magicc


def test_missing_custom_path():
    new_folder = "/my/root"
    try:
        m = magicc.MAGICC6_SCM(root_dir=new_folder)
        with pytest.raises(FileNotFoundError):
            m.instance()
    finally:
        m.cleanup()


@pytest.mark.parametrize("magicc_cls", [magicc.MAGICC6_SCM, magicc.MAGICC7_SCM])
def test_magicc_output(magicc_cls):
    with magicc_cls(only=("Surface Temperature",), out_temperature=1) as scm:
        try:

            res = scm.run({})

            assert isinstance(res, MAGICCData)
        except WineNotInstalledError:
            pytest.skip("Wine not installed")


@pytest.mark.parametrize("magicc_cls", [magicc.MAGICC6_SCM, magicc.MAGICC7_SCM])
def test_magicc_merge_config(magicc_cls):
    scm = magicc_cls(config_param="test", extra_param=0)
    with patch.object(scm, "instance") as mock_magicc:
        mock_magicc.return_value.out_dir = "/tmp/notexists"
        scm.run({"extra_param": 1})

        args, kwargs = mock_magicc().run.call_args
        assert "extra_param" in kwargs
        assert kwargs["extra_param"] == 1
        assert kwargs["config_param"] == "test"


@pytest.mark.parametrize(
    "cls",
    [
        magicc.MAGICC7_SCM,
        magicc.MAGICC7_Parallel,
        magicc.MAGICC6_SCM,
        magicc.MAGICC6_Parallel,
    ],
)
def test_single(cls, tmp_path):
    try:
        with cls(root_dir=tmp_path) as scm:
            res = scm.run(
                {"core_climatesensitivity": 2.5}, iter_over_value={"run_num": 1}
            )
            assert len(listdir(tmp_path)) == 1
            assert len(res["run_num"].unique()) == 1

        # Check that things have been cleaned up
        assert len(listdir(tmp_path)) == 0
    except WineNotInstalledError:
        pytest.skip("Wine not installed")


@pytest.mark.parametrize(
    "cls",
    [
        magicc.MAGICC7_SCM,
        magicc.MAGICC7_Parallel,
        magicc.MAGICC6_SCM,
        magicc.MAGICC6_Parallel,
    ],
)
def test_single_failed(cls, tmp_path):
    try:
        with cls(root_dir=tmp_path) as scm:
            with pytest.raises(ValueError):
                scm.run({"test": 2.5})

        # Check that things have been cleaned up even with a failure
        assert len(listdir(tmp_path)) == 0
    except WineNotInstalledError:
        pytest.skip("Wine not installed")


@pytest.mark.parametrize(
    "cls",
    [
        magicc.MAGICC7_SCM,
        magicc.MAGICC7_Parallel,
        magicc.MAGICC6_SCM,
        magicc.MAGICC6_Parallel,
    ],
)
def test_multiple_failed(cls, tmp_path):
    try:
        with cls(root_dir=tmp_path) as scm:
            with pytest.raises(ValueError):
                scm.run_multiple(
                    [
                        ({"test": 2.5}, {"run_num": 1}),
                        ({"core_climatesensitivity": 3}, {"run_num": 2}),
                    ]
                )

        # Check that things have been cleaned up even with a failure
        assert len(listdir(tmp_path)) == 0
    except WineNotInstalledError:
        pytest.skip("Wine not installed")


def test_serial_magicc(tmp_path):
    with magicc.MAGICC7_SCM(root_dir=tmp_path) as scm:
        res = scm.run_multiple(
            [
                ({"core_climatesensitivity": 2.5}, {"run_num": 1}),
                ({"core_climatesensitivity": 3}, {"run_num": 2}),
            ]
        )
        assert len(listdir(tmp_path)) == 1
        assert len(res["run_num"].unique()) == 2

    # Check that things have been cleaned up
    assert len(listdir(tmp_path)) == 0


def test_parallel_magicc(tmp_path):
    with magicc.MAGICC7_Parallel(root_dir=tmp_path, n_workers=2) as scm:
        res = scm.run_multiple(
            [
                ({"core_climatesensitivity": 2.5}, {"run_num": 1}),
                ({"core_climatesensitivity": 3}, {"run_num": 2}),
            ]
        )
        instances = listdir(tmp_path)
        assert len(instances) == 2
        assert len(res["run_num"].unique()) == 2

        # Running multiple runs again doesn't create new instances
        res = scm.run_multiple(
            [
                ({"core_climatesensitivity": 2.5}, {"run_num": 1}),
                ({"core_climatesensitivity": 3}, {"run_num": 2}),
            ]
        )
        assert listdir(tmp_path) == instances
        assert len(res["run_num"].unique()) == 2

    # Check that things have been cleaned up
    assert len(listdir(tmp_path)) == 0


@pytest.mark.parametrize("cls", [magicc.MAGICC7_Parallel, magicc.MAGICC6_Parallel])
def test_parallel_uninitialised(cls):
    m = magicc.MAGICC7_Parallel()
    with pytest.raises(
        ValueError, match="run_multiple called outside of a context manager"
    ):
        m.run_multiple([])
