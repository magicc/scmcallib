from scmcallib.distributions import Normal, Bound, Scalar, Uniform
import pytest

def test_repr():
    n = Normal()

    assert Normal().__repr__() == "<Normal mu=0.0, sigma=1.0>"
    assert Normal(mu=1.0).__repr__() == "<Normal mu=1.0, sigma=1.0>"
    assert Normal(mu=1.0, sigma=20).__repr__() == "<Normal mu=1.0, sigma=20>"
    assert Normal(something=100).__repr__() == "<Normal mu=0.0, sigma=1.0>"


def test_bounded_variable():
    lower = 1
    upper = 2

    b = Bound(Normal(mu=1.5, sigma=5), lower=lower, upper=upper)
    assert b.__repr__() == "<Bound dist=<Normal mu=1.5, sigma=5>, lower=1, upper=2>"
    samples = b.evaluate(1000)
    assert samples.min() >= lower
    assert samples.max() <= upper

    b = Bound(Normal, lower=lower, upper=upper)
    assert b.__repr__() == "<Bound dist=Normal, lower=1, upper=2>"
    b2 = b(mu=1.5, sigma=5)
    assert b2.__repr__() == "<Bound dist=<Normal mu=1.5, sigma=5>, lower=1, upper=2>"
    assert b2.kwargs["lower"] == 1
    assert b2.kwargs["upper"] == 2
    assert b2.distribution.kwargs["mu"] == 1.5
    assert b2.distribution.kwargs["sigma"] == 5

    samples = b2.evaluate(1000)
    assert samples.min() >= lower
    assert samples.max() <= upper


@pytest.mark.parametrize("Dist,values", (
    (Scalar, {"value": 100}),
    (Normal, {"mu": 1, "sigma": 3}),
    (Uniform, {"lower": 10, "upper": 20}),
))
def test_dist_args_kwargs(Dist, values):
    s = Dist(*list(values.values()))

    for v in values:
        assert getattr(s, v) == values[v]
