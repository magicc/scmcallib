import numpy as np
import pandas as pd
import pytest

from scmcallib.distributions import Scalar, Uniform, Normal
from scmcallib.parameterset import ParameterSet, ScenarioParameterSet, KeyedParameterSet


@pytest.fixture(scope="function")
def ps():
    s = KeyedParameterSet({"scenario": ["A", "B"], "model": [1, 2]})

    s.set_config("test", 1)
    s.set_config("test_variable", {"A": "v1", "B": "v2"}, ("scenario",))
    s.set_config("test_variable2", {1: "v3", 2: "v4"}, ("model",))
    s.set_config("test_variable3", {("A", 1): "v5", ("B", 2): "v6"}, ("scenario", "model",))

    return s


def test_keyed_multi_dim(ps):
    assert ps.state == {"scenario": "A", "model": 1}

    res = ps.config_parameters
    res2 = ps._get_config_parameters(ps.state)
    assert res == res2
    assert dict(res) == {
        "test": 1,
        "test_variable": "v1",
        "test_variable2": "v3",
        "test_variable3": "v5"
    }

    res = ps._get_config_parameters({
        "scenario": "B",
        "model": 2
    })
    assert dict(res) == {
        "test": 1,
        "test_variable": "v2",
        "test_variable2": "v4",
        "test_variable3": "v6"
    }

    res = ps._get_config_parameters({
        "scenario": "B",
        "model": 1
    })
    assert dict(res) == {
        "test": 1,
        "test_variable": "v2",
        "test_variable2": "v3",
        "test_variable3": None
    }


def test_keyed_multi_dim_iter(ps):
    res = []
    for k in ps:
        res.append((ps.state, k))

    # iterates scenario and then model (reverse alphabetical order)
    # [("A", 1), ("B", 1), ("A", 2), ("B", 2)]
    assert len(res) == 4
    assert (dict(res[1][0]), dict(res[1][1])) == (
        {"scenario": "B", "model": 1},
        {
            "test": 1,
            "test_variable": "v2",
            "test_variable2": "v3",
            "test_variable3": None,
        }
    )


def test_keyed_multi_dim_df(ps):
    res = ps.to_df()

    idx = pd.MultiIndex.from_product([[1, 2], ["A", "B"]], names=("model", "scenario"))
    exp = pd.DataFrame(
        np.asarray([[1, 1, 1, 1], ["v1", "v2", "v1", "v2"], ["v3", "v3", "v4", "v4"], ["v5", None, None, "v6"]]).T,
        columns=["test", "test_variable", "test_variable2", "test_variable3"],
        index=idx
    )

    pd.testing.assert_frame_equal(res, exp, check_dtype=False)


def test_keyed():
    s = ScenarioParameterSet(["A", "B"])

    assert s.state == {"scenario": "A"}
    s.set_config("TEST", "BOTH")
    s.set_config("OTHER", {"A": "SCEA", "B": "SCEB"})

    assert dict(s.config_parameters) == {"TEST": "BOTH", "OTHER": "SCEA"}

    s.state = {"scenario": "B"}

    assert dict(s.config_parameters) == {"TEST": "BOTH", "OTHER": "SCEB"}

    assert dict(s.model_specific_parameters) == {"OTHER": "SCEB"}


def test_keyed_missing():
    s = ScenarioParameterSet(["A", "B"])
    s.set_config("TEST", {})

    with pytest.warns(UserWarning):
        r = s.config_parameters

    assert len(r) == 1
    assert r["TEST"] is None


def test_evaluate():
    s = ParameterSet()

    df = s.evaluate(100)

    assert len(df) == 100


def test_evaluate_include_config():
    s = ParameterSet()
    s.set_tune("test", Uniform(0, 5))
    s.set_config("test-config", 1)

    df = s.evaluate(1)

    assert "test" in df
    assert "test-config" not in df

    df = s.evaluate(1, include_config=True)
    assert "test" in df
    assert "test-config" in df


def test_run_id_increments():
    s = ParameterSet()

    df1 = s.evaluate(100)
    df2 = s.evaluate(100)

    for run_num in df1.index:
        assert run_num not in df2.index


def test_param_adds_column():
    s = ParameterSet()
    s.set_tune("test", Scalar(4))

    df = s.evaluate(10)
    assert "test" in df


def test_config_param_doesnt_adds_column():
    s = ParameterSet()
    s.set_tune("test", Scalar(4))
    s.set_config("test-config", 4)

    df = s.evaluate(10)
    assert "test-config" not in df


def test_only_one_param():
    s = ParameterSet()
    s.set_tune("test", Scalar(4))
    with pytest.raises(ValueError):
        s.set_config("test", 1)

    s.set_config("test-config", 4)
    with pytest.raises(ValueError):
        s.set_tune("test-config", Scalar(1))


def test_config_param_func():
    s = ParameterSet()
    s.set_config("test", lambda scenario: "test_{}".format(scenario))

    with pytest.warns(Warning, match=r"could not evaluate config parameter `test`"):
        assert len(s.config_parameters) == 0

    s.state = {"scenario": "rcp26"}

    assert s.config_parameters["test"] == "test_rcp26"


@pytest.mark.parametrize("inp", (4, 4.0, -1, [1], "4", None))
def test_non_dist(inp):
    s = ParameterSet()
    with pytest.raises(ValueError):
        s.set_tune("test", inp)


def test_evaluate_lhs():
    s = ParameterSet()
    a = Uniform(lower=2, upper=4)
    b = Uniform(lower=-1, upper=1)

    s.set_tune("a", a)
    s.set_tune("b", b)

    samples = s.evaluate(1000, method="lhs")
    assert len(samples) == 1000

    assert (samples["a"] > 2).all()
    assert (samples["a"] < 4).all()

    assert (samples["b"] > -1).all()
    assert (samples["b"] < 1).all()


def test_evaluate_lhs_nonuniform():
    s = ParameterSet()
    a = Normal(mu=2, sigma=4)
    b = Uniform(lower=-1, upper=1)

    s.set_tune("a", a)
    s.set_tune("b", b)
    with pytest.raises(ValueError):
        s.evaluate(100, method="lhs")


def test_evaluate_unknown():
    s = ParameterSet()
    with pytest.raises(ValueError):
        s.evaluate(100, method="unknown")
