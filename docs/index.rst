Welcome to SCM Calibration's documentation!
===========================================

.. include:: ../README.rst
    :start-after: sec-begin-index
    :end-before: sec-end-index

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    installation
    usage
    terms
    contributing

.. toctree::
    :maxdepth: 2
    :caption: API reference

    scmcallib.distributions
    scmcallib.finder
    scmcallib.metrics
    scmcallib.optimiser
    scmcallib.parameterset
    scmcallib.scm
    scmcallib.transforms
    scmcallib.tuningcore
    scmcallib.utils

.. toctree::
    :maxdepth: 2
    :caption: Versions

    changelog

Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
